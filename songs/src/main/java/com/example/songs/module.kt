package com.example.songs

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val songsModule = module {
    viewModel { SongsViewModel() }
}