package com.example.songs

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.screenprovider.IScreenProvider
import kotlinx.android.synthetic.main.fragment_songs.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class SongsFragment : Fragment(R.layout.fragment_songs) {

    private val screenProvider: IScreenProvider by inject()
    private val viewModel: SongsViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        button.setOnClickListener {
            startActivity(
                screenProvider.getAboutActivityIntent(requireContext())
            )
        }

        button2.setOnClickListener {
            val fragment = screenProvider.getSongInfoFragment(
                button2.text.toString(),
                this,
                100,
                RESULT_KEY
            )

            requireFragmentManager().beginTransaction()
                .replace(screenProvider.mainContainerViewId, fragment)
                .addToBackStack(null)
                .commit()

        }

        button3.setOnClickListener {

            val fragment = screenProvider.getSongInfoFragment(
                button3.text.toString(),
                this,
                100,
                RESULT_KEY
            )

            requireFragmentManager().beginTransaction()
                .replace(screenProvider.mainContainerViewId, fragment)
                .addToBackStack(null)
                .commit()
        }

        button4.setOnClickListener {

            val fragment = screenProvider.getSongInfoFragment(
                button4.text.toString(),
                this,
                100,
                RESULT_KEY
            )

            requireFragmentManager().beginTransaction()
                .replace(screenProvider.mainContainerViewId, fragment)
                .addToBackStack(null)
                .commit()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.playingSong.observe(viewLifecycleOwner, Observer {
            textView.text = "Now playing: $it"
            textView.setTextColor(Color.RED)
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            data?.getStringExtra(RESULT_KEY)?.let { viewModel.playSong(it) }
        }
    }

    companion object {
        private const val RESULT_KEY = "RESULT_KEY"
    }

}