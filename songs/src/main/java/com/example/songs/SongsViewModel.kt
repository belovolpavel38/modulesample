package com.example.songs

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SongsViewModel : ViewModel() {

    private val _playingSong = MutableLiveData<String>()
    val playingSong : LiveData<String> = _playingSong

    fun playSong(song: String) {
        _playingSong.value = song
    }

}