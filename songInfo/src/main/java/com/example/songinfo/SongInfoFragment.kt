package com.example.songinfo

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_song_info.*

private const val ARG_SONG = "ARG_SONG"
private const val ARG_REQUEST_CODE = "ARG_REQUEST_CODE"
private const val ARG_RESULT_KEY = "ARG_RESULT_KEY"

class SongInfoFragment : Fragment(R.layout.fragment_song_info) {

    private var song: String? = null
    private var requestCode: Int? = null
    private var resultKey: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            song = it.getString(ARG_SONG)
            requestCode = it.getInt(ARG_REQUEST_CODE)
            resultKey = it.getString(ARG_RESULT_KEY)
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        textView.text = song
        button.setOnClickListener {
            targetFragment?.onActivityResult(
                requestCode!!,
                Activity.RESULT_OK,
                Intent().apply {
                    putExtra(resultKey, song)
                })
            fragmentManager?.popBackStack()
        }
    }

    companion object {

        fun newInstance(
            song: String,
            targetFragment: Fragment,
            requestCode: Int,
            resultKey: String
        ) = SongInfoFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_SONG, song)
                putInt(ARG_REQUEST_CODE, requestCode)
                putString(ARG_RESULT_KEY, resultKey)
            }
            setTargetFragment(targetFragment, requestCode)
        }
    }
}