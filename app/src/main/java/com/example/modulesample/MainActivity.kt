package com.example.modulesample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.songs.SongsFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, SongsFragment())
                .addToBackStack(null)
                .commit()
        }

    }


    override fun onBackPressed() {
        if(supportFragmentManager.backStackEntryCount > 1) {
            super.onBackPressed()
        }
    }
}