package com.example.modulesample

import android.app.Application
import android.content.Context
import android.content.Intent
import androidx.fragment.app.Fragment
import com.example.screenprovider.IScreenProvider
import com.example.songinfo.SongInfoFragment
import com.example.songs.SongsFragment
import com.example.songs.SongsViewModel
import com.example.songs.songsModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(
                listOf(
                    songsModule,
                    screenModule
                )
            )
        }
    }

    private val screenModule = module {
        single<IScreenProvider> {
            ScreenProvider()
        }
    }

    class ScreenProvider : IScreenProvider {

        override val mainContainerViewId: Int
            get() = R.id.container

        override fun getAboutActivityIntent(context: Context) =
            Intent(context, AboutActivity::class.java)

        override fun getSongInfoFragment(
            song: String,
            targetFragment: Fragment,
            requestCode: Int,
            resultKey: String
        ): Fragment = SongInfoFragment.newInstance(song, targetFragment, requestCode, resultKey)

    }

}