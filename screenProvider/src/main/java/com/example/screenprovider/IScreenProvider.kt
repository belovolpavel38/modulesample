package com.example.screenprovider

import android.content.Context
import android.content.Intent
import androidx.fragment.app.Fragment

interface IScreenProvider {

    val mainContainerViewId: Int

    fun getAboutActivityIntent(context: Context): Intent

    fun getSongInfoFragment(
        song: String,
        targetFragment: Fragment,
        requestCode: Int,
        resultKey: String
    ): Fragment
}